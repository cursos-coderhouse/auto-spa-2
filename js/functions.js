function fillFullCatalog() {
    const catalog = new Catalog();
    products.forEach(product => {
        catalog.addProduct(product);
    });
    catalog.shuffle();
    return catalog;
}

function buildCatalog(catalog) {
    const $salesRef = $('#sales');
    const $regularRef = $('#regular');
    $salesRef.html('');
    $regularRef.html('');
    let sCounter = 0;
    let rCounter = 0;
    for (let i = 0; i < catalog.getProducts().length; i++) {
        const actualProduct = catalog.getProducts()[i];
        if (actualProduct.stock > 0) {
            if (actualProduct.onSale) {
                sCounter++;
                $salesRef.append(`
                    <div class="col-3">
                        <button class="btn" type="button">
                            <img src=${actualProduct.imagePath} id=${"img" + actualProduct.id} class="img-fluid border border-dark rounded-lg w-100" data-id=${actualProduct.id} style="height: auto;">
                        </button>
                        <h5 class="mt-1 mb-1">${actualProduct.name}</h5>
                        <p style="margin: 0px; color: red;">
                            <s>${formatPrice(actualProduct.price)}</s>
                        </p>
                        <p>${formatPrice(actualProduct.salePrice)}</p>
                        <button type="button" id=${"add" + actualProduct.id} class="btn mt-n2 mb-3" data-id=${actualProduct.id} style="background-color: black;">
                            <img src="assets/add-to-cart.png" data-id=${actualProduct.id} style="height: 25px;">
                        </button>
                    </div>`);
            } else {
                rCounter++;
                $regularRef.append(`
                    <div class="col-3">
                        <button class="btn" type="button">
                            <img src=${actualProduct.imagePath} id=${"img" + actualProduct.id} class="img-fluid border border-dark rounded-lg w-100" data-id=${actualProduct.id} style="height: auto;">
                        </button>
                        <h5 class="mt-1 mb-1">${actualProduct.name}</h5>
                        <p>${formatPrice(actualProduct.price)}</p>
                        <button type="button" id=${"add" + actualProduct.id} class="btn mt-n2 mb-3" data-id=${actualProduct.id} style="background-color: black;">
                            <img src="assets/add-to-cart.png" data-id=${actualProduct.id} style="height: 25px;">
                        </button>
                    </div>`);
            }
        } else {
            rCounter++;
            $regularRef.append(`
                <div class="col-3">
                    <button class="btn" type="button">
                        <div style="position: relative;">
                            <img src=${actualProduct.imagePath} class="img-fluid border border-dark rounded-lg w-100" style="height: auto;">
                            <img src="assets/no_stock.gif" id=${"img" + actualProduct.id} class="img-fluid" data-id=${actualProduct.id} style="height: auto; position: absolute; top: 0px; left: 0px;">
                        </div>
                    </button>
                    <h5 class="mt-1 mb-1">${actualProduct.name}</h5>
                    <p>${formatPrice(actualProduct.price)}</p>
                    <button type="button" class="btn mt-n2 mb-3" style="background-color: black;" disabled>
                        <img src="assets/add-to-cart.png" style="height: 25px;">
                    </button>
                </div>`);
        }
    }
    for (let i = 0; i < catalog.getProducts().length; i++) {
        const actualProduct = catalog.getProducts()[i];
        $('#img' + actualProduct.id).click(() => {
            fillProductView(findProduct(actualProduct.id));
            $('#productModal').modal('toggle');
        });
        $('#add' + actualProduct.id).click(event => {
            cart.addProduct(findProduct(event.target.dataset.id));
            save(cart.getProducts());
            buildCart(cart);
            $('#itemsCounter').text(cart.getProductsQuantity());
        });
    }
    if (sCounter === 0) {
        $salesRef.append(`<div class="container text-center"><p style="font-size: medium;"><b>No hay productos para mostrar.</b></p></div>`);
    }
    if (rCounter === 0) {
        $regularRef.append(`<div class="container text-center"><p style="font-size: medium;"><b>No hay productos para mostrar.</b></p></div>`);
    }
}

function findProduct(id) {
    let ret = {};
    products.forEach(product => {
        if (product.id === id) {
            ret = product;
        }
    });
    return ret;
}

function fillProductView(product) {
    $('#name').text(product.name);
    $('#image').attr('src', () => {
        return product.imagePath;
    });
    $('#description').html(product.description);
    if (product.onSale) {
        $('#sale-price').html(`<s>${formatPrice(product.price)}</s>`);
        $('#price').text(formatPrice(product.salePrice));
    } else {
        $('#sale-price').html('');
        $('#price').text(formatPrice(product.price));
    }
    $('#addToCartBtn').replaceWith($('#addToCartBtn').clone(false));
    if (product.stock > 0) {
        $('#addToCartBtn').attr('disabled', false);
        $('#addToCartBtn').click(() => {
            cart.addProduct(findProduct(product.id));
            save(cart.getProducts());
            buildCart(cart);
            $('#itemsCounter').text(cart.getProductsQuantity());
            $('#productModal').modal('toggle');
        });
    } else {
        $('#addToCartBtn').attr('disabled', true);
    }
}

function buildCart(cart) {
    const $listRef = $('#list');
    $listRef.html('');
    if (cart.getProductsQuantity() === 0) {
        $listRef.append(`<li class="list-group-item"><p style="font-size: medium;"><b>No hay productos para mostrar.</b></p></li>`);
    } else {
        $listRef.append(`
            <div class="row justify-content-between mt-n2 mb-n2 ml-2 mr-2">
                <div class="d-flex">
                    <p class="font-weight-bold mr-5">Selec.</p>
                    <p class="font-weight-bold text-right">Producto (precio)</p>
                </div>
                <p class="font-weight-bold">Cantidad</p>
            </div>`);
        for (let i = 0; i < cart.getProductsQuantity(); i++) {
            if (!wasAlreadyDisplayed(cart.getProducts(), i)) {
                const actualProduct = cart.getProducts()[i];
                let label = '';
                if (actualProduct.onSale) {
                    label = `${actualProduct.name} (${formatPrice(actualProduct.salePrice)})`;
                } else {
                    label = `${actualProduct.name} (${formatPrice(actualProduct.price)})`;
                }
                $listRef.append(`
                    <li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                        <div>
                            <input type="checkbox" value=${actualProduct.id} id="productCheck" class="mr-5">
                            <label class="m-1">${label}</label>
                        </div>
                        <span class="badge badge-primary badge-pill">${cart.getQuantity(actualProduct)}</span>
                    </li>`);
            }
        }
    }
    $listRef.append(
        `<li class="list-group-item">
            <div class="row justify-content-between m-0 align-items-center">
                <p style="font-size: x-large;">
                    <b>Total: ${formatPrice(cart.getTotalPrice())}</b>
                </p>
                <button type="button" id="emptyCartBtn" class="btn btn-secondary">Vaciar carrito</button>
                <button type="button" id="deleteProductBtn" class="btn btn-danger">Eliminar producto/s</button>
            </div>
        </li>`);
    $('#emptyCartBtn').click(() => {
        cart.empty();
        save(cart.getProducts());
        buildCart(cart);
        $('#itemsCounter').text(cart.getProductsQuantity());
    });
    $('#deleteProductBtn').click(() => {
        const checkedProducts = document.querySelectorAll('#productCheck:checked');
        checkedProducts.forEach(checkbox => {
            cart.deleteProduct(checkbox.value);
        });
        save(cart.getProducts());
        buildCart(cart);
        $('#itemsCounter').text(cart.getProductsQuantity());
    });
}

function wasAlreadyDisplayed(array, index) {
    for (let i = 0; i < index; i++) {
        if (array[i].id === array[index].id) {
            return true;
        }
    }
    return false;
}

function formatPrice(price) {
    const split = price.toString().split('.');
    split[0] = insertDot(split[0]);
    if (split[1] === undefined) {
        return `$ ${split[0]},00`;
    }
    return `$ ${split[0]},${split[1]}`;
}

function insertDot(string) {
    let aux = '';
    let ret = '';
    let counter = 3;
    for (let i = string.length - 1; i >= 0; i--) {
        if (counter === 0) {
            aux += '.';
            counter = 3;
        }
        counter--;
        aux += string[i];
    }
    for (let i = aux.length - 1; i >= 0; i--) {
        ret += aux[i];
    }
    return ret;
}

function filter(catalog, string) {
    const newCatalog = new Catalog();
    for (let i = 0; i < catalog.getProducts().length; i++) {
        const actualProduct = catalog.getProducts()[i];
        for (let j = 0; j < actualProduct.tags.length; j++) {
            const actualTag = actualProduct.tags[j];
            if (actualTag.includes(string.toLowerCase())) {
                newCatalog.addProduct(actualProduct);
                break;
            }
        }
    }
    return newCatalog;
}

function save(data) {
    localStorage.setItem(KEY_STORAGE, JSON.stringify(data));
}

function load(storageKey) {
    if (localStorage.getItem(storageKey) != null) {
        return JSON.parse(localStorage.getItem(storageKey));
    } else {
        return [];
    }
}

function compareNames(a, b) {
    if (a.nombre < b.nombre) {
        return -1;
    } else if (a.nombre > b.nombre) {
        return 1;
    } else {
        return 0;
    }
}

function toggleGenericModal(title, img, width, p) {
    $('#genericModalTitle').html(title);
    $('#genericModalBody').html(`
        <div class="mb-3 text-center">
            <img src='./assets/${img}' width=${width}>
        </div>
        <p class="m-2">${p}</p>`);
    $('#genericModal').modal('toggle');
}