const Catalog = function () {
    let products = [];

    this.addProduct = function (product) {
        products.push(product);
    }

    this.getProducts = () => products;

    this.shuffle = function () {
        for (let i = products.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * i);
            const temp = products[i];
            products[i] = products[j];
            products[j] = temp;
        }
    }
}

const Cart = function () {
    let products = [];

    this.addProduct = function (product) {
        products.push(product);
    }

    this.deleteProduct = function (id) {
        for (let i = products.length - 1; i >= 0; i--) {
            if (products[i].id === id) {
                products.splice(i, 1);
                break;
            }
        }
    }

    this.empty = function () {
        products = [];
    }

    this.getTotalPrice = function () {
        let acum = 0;
        products.forEach(function (product) {
            if (product.onSale) {
                acum += product.salePrice;
            } else {
                acum += product.price;
            }
        });
        return acum.toFixed(2);
    }

    this.getQuantity = function (product) {
        let acum = 0;
        for (let i = 0; i < products.length; i++) {
            if (products[i].id === product.id) {
                acum++;
            }
        }
        return acum;
    }

    this.getProducts = () => products;

    this.getProductsQuantity = () => products.length;

    this.setProducts = function (array) {
        products = array;
    }
}