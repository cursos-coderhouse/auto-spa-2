let products = [];
let index = {};
const cart = new Cart();
const KEY_STORAGE = 'cartData';

$('document').ready(() => {
    const $catalogRef = $('#catalog');

    $.ajax({
        method: "GET",
        url: './data/products.json',
    }).done(data => {
        products = data;
        index = fillFullCatalog()
        buildCatalog(index);
    }).fail(error => {
        console.log(error);
    });

    cart.setProducts(load(KEY_STORAGE));
    buildCart(cart);
    $('#itemsCounter').text(cart.getProductsQuantity());

    $('#searchForm').submit(event => {
        event.preventDefault();
        if (event.target.criteria.value.length > 1) {
            if ($('#searchCriteria').html() != undefined) {
                $('#searchCriteria').remove();
            }
            $catalogRef.prepend(`
                <div id="searchCriteria" class="row">
                    <p class="h4 font-weight-light mb-3">Búsqueda: ${event.target.criteria.value}</p>
                </div>`);
            index = filter(fillFullCatalog(), event.target.criteria.value);
            buildCatalog(index);
        } else {
            toggleGenericModal('¡Búsqueda inválida!', 'bad-search.png', '50', 'Escribí en el buscador lo que querés encontrar <b>(las búsquedas deben contener más de una letra)</b>.');
        }
    });
    $('#indexBtn').click(() => {
        if ($('#searchCriteria').html() != undefined) {
            $('#searchCriteria').remove();
        }
        index = fillFullCatalog();
        buildCatalog(index);
    });
    $('.categoryItem').click(event => {
        if ($('#searchCriteria').html() != undefined) {
            $('#searchCriteria').remove();
        }
        $catalogRef.prepend(`
            <div id="searchCriteria" class="row">
                <p class="h4 font-weight-light mb-3">Filtro: ${event.target.dataset.value}s</p>
            </div>`);
        index = filter(fillFullCatalog(), event.target.dataset.value);
        buildCatalog(index);
    });
    $('#cartBtn').click(() => {
        $('#cartModal').modal('toggle');
    });
    $('#nextBtn').click(() => {
        if (cart.getProductsQuantity() > 0) {
            $('#cartModal').modal('toggle');
            $('#paymentModal').modal('toggle');
        } else {
            $('#cartModal').modal('toggle');
            toggleGenericModal('¡Tu carrito está vacío!', 'alert.png', '50', 'Debes añadir al menos 1 producto para continuar la compra.');
        }
    });
    $.ajax({
        method: "GET",
        url: 'https://apis.datos.gob.ar/georef/api/provincias?campos=id,nombre',
    }).done(data => {
        data.provincias.sort(compareNames).forEach(provincia => {
            $('#inputState').append(`<option value="${provincia.id}">${provincia.nombre}</option>`);
        });
    }).fail(error => {
        console.log(error);
    });
    $('#inputState').change(event => {
        $('#inputCity').html('<option selected value="">Seleccione una ciudad</option>');
        if (event.target.value != 02) {
            $.ajax({
                method: "GET",
                url: `https://apis.datos.gob.ar/georef/api/municipios?provincia=${event.target.value}&campos=id,nombre&max=100`,
            }).done(data => {
                data.municipios.sort(compareNames).forEach(municipio => {
                    $('#inputCity').append(`<option value="${municipio.id}">${municipio.nombre}</option>`);
                });
            }).fail(error => {
                console.log(error);
            });
        } else {
            $('#inputCity').append(`<option value="${event.target.value}">Ciudad Autónoma de Buenos Aires</option>`);
        }
    });
    $('#backBtn').click(() => {
        $('#paymentModal').modal('toggle');
        $('#paymentForm').trigger("reset");
        $('#paymentForm').validate().resetForm();
        $('#cartModal').modal('toggle');
    });
    $('#buyBtn').click(() => {
        $('#paymentForm').validate({
            rules: {
                cardNumber: {
                    required: true,
                    minlength: 13,
                    maxlength: 18
                },
                cardCode: {
                    required: true,
                    minlength: 3,
                    maxlength: 3,
                    digits: true
                },
                cardDate: "required",
                surnames: "required",
                names: "required",
                address: "required",
                city: "required",
                state: "required",
                zip: "required",
                phone: {
                    required: true
                }
            },
            messages: {
                cardNumber: {
                    required: 'El campo Número de tarjeta es obligatorio',
                    minlength: 'El número debe tener entre 13 y 18 dígitos',
                    maxlength: 'El número debe tener entre 13 y 18 dígitos'
                },
                cardCode: {
                    required: 'El campo Código de seguridad es obligatorio',
                    minlength: 'El número debe ser de 3 dígitos',
                    maxlength: 'El número debe ser de 3 dígitos',
                    digits: 'El código debe ser numérico'
                },
                cardDate: {
                    required: 'El campo Fecha de vencimiento es obligatorio'
                },
                surnames: {
                    required: 'El campo Apellido/s es obligatorio'
                },
                names: {
                    required: 'El campo Nombre/s es obligatorio'
                },
                address: {
                    required: 'El campo Dirección es obligatorio'
                },
                city: {
                    required: 'El campo Ciudad es obligatorio'
                },
                state: {
                    required: 'El campo Provincia es obligatorio'
                },
                zip: {
                    required: 'El campo Código postal es obligatorio'
                },
                phone: {
                    required: 'El campo Teléfono de contacto es obligatorio'
                }
            },
            submitHandler: () => {
                $('#paymentModal').modal('toggle');
                cart.empty();
                save(cart.getProducts());
                buildCart(cart);
                $('#itemsCounter').text(cart.getProductsQuantity());
                $('#successModal').modal('toggle');
                $('#paymentForm').trigger("reset");
            },
            errorClass: "errorTxt"
        });
    });
    $('#usBtn').click(event => {
        event.preventDefault();
        toggleGenericModal('Acerca de nosotros', 'autospa-inverted.png', '200', `<b>Auto Spa</b> es un sitio de venta de productos destinados al auto-detailing, que nació como un emprendimiento y se expandió gracias al arduo trabajo y el apoyo de los clientes.
        <br><br>¡Te invitamos a navegar por nuestro catálogo y comenzar con la experiencia Auto Spa!`);
    });
    $('#helpBtn').click(event => {
        event.preventDefault();
        toggleGenericModal('¿En qué podemos ayudarte?', 'help.png', '50', `Nuestro soporte en tiempo real aún no esta disponible, te pedimos disculpas.
        <br><br><b>¡Pero pará!</b> Clickeando en el botón "<b>Contáctanos</b>" ubicado al pie del sitio podés escribirnos tu consulta o problema y te responderemos en breve.
        <br><br><b>¡Muchas gracias!</b>`);
    });
    $('#contactBtn').click(event => {
        event.preventDefault();
        $('#contactModal').modal('toggle');
    });
    $('#closeBtn').click(() => {
        $('#contactModal').modal('toggle');
        $('#contactForm').trigger("reset");
        $('#contactForm').validate().resetForm();
    });
    $('#sendBtn').click(() => {
        $('#contactForm').validate({
            rules: {
                names: "required",
                surnames: "required",
                email: {
                    required: true,
                    email: true
                },
                query: "required"
            },
            messages: {
                names: {
                    required: 'El campo Nombre/s es obligatorio'
                },
                surnames: {
                    required: 'El campo Apellido/s es obligatorio'
                },
                email: {
                    required: 'El campo Email es obligatorio',
                    email: 'El mail ingresado es inválido.'
                },
                query: {
                    required: '¡No te olvides de hacernos tu consulta!'
                }
            },
            submitHandler: () => {
                $('#contactModal').modal('toggle');
                toggleGenericModal('Consulta enviada', 'success.png', '50', '<b>¡Gracias por contactarnos!</b><br><br>Te enviaremos una respuesta a la dirección de mail indicada.');
                $('#contactForm').trigger("reset");
            },
            errorClass: "errorTxt"
        });
    });
    $('#policiesBtn').click(event => {
        event.preventDefault();
        toggleGenericModal('Políticas de servicio', 'policies.png', '75', `El equipo de <b>Auto Spa</b> está enfocado en:<br>
        <br><b>1.</b> Responder oportunamente a las necesidades de los clientes.
        <br><b>2.</b> Ofrecer un óptimo servicio con un amplio portafolio de productos.
        <br><b>3.</b> Conocer las necesidades y expectativas del cliente interno y externo.
        <br><b>4.</b> Comprometidos con ser la mejor operación, pondremos a disposición todo nuestro talento humano, en un proceso de mejoramiento continuo.
        <br><b>5.</b> Crear espacios en los que las opiniones de nuestros clientes sean el precedente para nuestro óptimo desarrollo.</p>`);
    });
    $('#topBtn').click(() => {
        $('html, body').animate({ scrollTop: 0 }, 800);
    });
});